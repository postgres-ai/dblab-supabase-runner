/**
 * This is a Node.js server designed to listen to DLE webhooks.
 * Upon receiving a webhook, it either creates or deletes Docker containers
 * that run Supabase components for database clones. It uses the dockerode library to
 * interface with Docker and the express library to handle HTTP requests.
 * Logging is facilitated by the log4js library.
 */

const express = require('express');
const Docker = require('dockerode');
const { exec } = require('child_process');
const log4js = require('log4js');
const dotenv = require('dotenv');

dotenv.config();

const app = express();
app.use(express.json());

const docker = new Docker();

const PORT = 8080;
const NETWORK_NAME = 'dblab_supabase_network';
const CONTAINER_RUN_TIMEOUT = 120000; // 2 minutes

const SECRET_TOKEN = process.env.SECRET_TOKEN;
const DB_PASSWORD = process.env.DB_PASSWORD;
const JWT_SECRET = process.env.JWT_SECRET;

// Setup logger with log4js
log4js.configure({
  appenders: {
    out: {
      type: 'stdout',
      layout: {
        type: 'pattern',
        pattern: '%d{yyyy-MM-dd hh:mm:ss} [%p] - %m',
      },
    },
  },
  categories: {
    default: {
      appenders: ['out'],
      level: 'debug',
    },
  },
});
const logger = log4js.getLogger();

// Create and start PostgREST container
async function createAndStartRestContainer(username, cloneName, port, dbname, restContainerName) {
  logger.info("Creating PostgREST container...");

  try {
    const container = await docker.createContainer({
      Image: 'postgrest/postgrest',
      Cmd: [],
      Env: [
        `PGRST_DB_URI=postgres://${username}:${DB_PASSWORD}@${cloneName}:${port}/${dbname}`,
        `PGRST_DB_SCHEMAS=public,storage,graphql_public`,
        `PGRST_DB_ANON_ROLE=anon`,
        `PGRST_JWT_SECRET=${JWT_SECRET}`,
        `PGRST_DB_USE_LEGACY_GUCS=false`
      ],
      ExposedPorts: { '3000/tcp': {} },
      HostConfig: {
        RestartPolicy: { Name: 'unless-stopped' },
        PortBindings: { '3000/tcp': [{ 'HostPort': `${port + 10000}` }] },
        NetworkMode: NETWORK_NAME,
      },
      name: restContainerName,
    });

    logger.info(`Container ${restContainerName} created, starting container...`);
    await container.start();

    logger.info(`Container ${restContainerName} started. Checking if it remains running...`);
    await checkContainerRunning(container, restContainerName);

    logger.info(`Container ${restContainerName} is still running!`);
  } catch (err) {
    logger.error("Error while creating or starting container:", err);
  }
}

// Function to check if a container is running
function checkContainerRunning(container, containerName, interval = 5000, timeout = CONTAINER_RUN_TIMEOUT) {
  return new Promise((resolve, reject) => {
    let timeoutId;
    let elapsedTime = 0;

    const checkInterval = setInterval(() => {
      container.inspect((err, _data) => {
        if (err) {
          clearInterval(checkInterval);
          clearTimeout(timeoutId);
          reject(err);
        } else if (_data.State.Running) {
          clearInterval(checkInterval);
          clearTimeout(timeoutId);
          resolve();
        } else if (!_data.State.Running && _data.State.Status === 'exited') {
          clearInterval(checkInterval);
          clearTimeout(timeoutId);
          reject(new Error(`Container ${containerName} has exited and is no longer running.`));
        } else if (elapsedTime >= timeout) {
          clearInterval(checkInterval);
          clearTimeout(timeoutId);
          reject(new Error(`Container ${containerName} failed to start within timeout ${timeout}.`));
        }
        elapsedTime += interval;
      });
    }, interval);

    timeoutId = setTimeout(() => {
      clearInterval(checkInterval);
      reject(new Error(`Container ${containerName} failed to start within timeout ${timeout}.`));
    }, timeout);
  });
}

// Define the POST route
app.post('/', function (req, res) {
  // Input validation
  if (!req.body.event_type || !req.body.container_name || !req.body.port || !req.body.username || !req.body.dbname) {
    logger.error("Missing required fields");
    return res.status(400).send("Missing required fields");
  }

  // Log the incoming request
  logger.info(`Received HTTP POST request: ${JSON.stringify(req.headers)}, ${req.method}, ${req.url}, ${JSON.stringify(req.body)}`);

  // Extract information from request
  const event_type = req.body.event_type;
  const cloneName = req.body.container_name;
  const port = req.body.port;
  const username = req.body.username;
  const dbname = req.body.dbname;
  const secret_token = req.headers['dblab-webhook-token'];

  // Construct the Supabase container name using the clone name
  const restContainerName = `${cloneName}_supabase_rest`;
  // TODO:
  // const studioContainerName = `${cloneName}_supabase_studio`;
  // const kongContainerName = `${cloneName}_supabase_kong`;
  // const authContainerName = `${cloneName}_supabase_auth`;
  // const realtimeContainerName = `${cloneName}_supabase_realtime`;
  // const storageContainerName = `${cloneName}_supabase_storage`;
  // const imgproxyContainerName = `${cloneName}_supabase_imgproxy`;
  // const metaContainerName = `${cloneName}_supabase_meta`;
  // const functionsContainerName = `${cloneName}_supabase_functions`;

  // Check for valid secret token
  if (secret_token !== SECRET_TOKEN) {
    logger.error(`Received incorrect secret token: ${secret_token}`);
    return res.status(403).send("Invalid secret token");
  }

  // Handle events
  if (event_type === "clone_create") {
    // Connect the Postgres container to the network
    // eslint-disable-next-line no-unused-vars
    exec(`docker network connect ${NETWORK_NAME} ${cloneName}`, (err, _stdout, _stderr) => {
      if (err) {
        logger.error(`Error connecting Postgres container to network: ${err}`);
      } else {
        logger.info(`Connected Postgres container ${cloneName} to network: ${NETWORK_NAME}`);
        createAndStartRestContainer(username, cloneName, port, dbname, restContainerName)
          .catch(err => logger.error(err));
      }
    });
  } else if (event_type === "clone_delete") {
    logger.info("Deleting PostgREST container...");

    // eslint-disable-next-line no-unused-vars
    docker.getContainer(restContainerName).inspect((err, _data) => {
      if (err) {
        logger.error(`Error finding container: ${err}`);
      } else {
        // eslint-disable-next-line no-unused-vars
        docker.getContainer(restContainerName).remove({ force: true }, (err, _data) => {
          if (err) {
            logger.error(`Error deleting container: ${err}`);
          } else {
            logger.info(`Container ${restContainerName} deleted successfully`);
          }
        });
      }
    });
  } else {
    logger.error(`Received incorrect event type: ${event_type}`);
    return res.status(400).send("Invalid event type");
  }

  res.sendStatus(200);
});

app.listen(PORT, function () {
  logger.info(`Server is running on port ${PORT}`);
});